#include <combspp/input.h>

namespace combspp {

    Input::Input(std::string_view s) :
        s{s},
        offset{0}
    {
    }

    Input::Input(const Input& other) :
        s{other.s},
        offset{other.offset}
    {
    }

    Input& Input::operator=(Input rhs) {
        std::swap(s, rhs.s);
        std::swap(offset, rhs.offset);
        return *this;
    }

    Input Input::operator++(int) {
        s = s.substr(1);
        offset += 1;
        return *this;
    }

    std::string_view Input::get() const {
        return s;
    }

    std::size_t Input::getOffset() const {
        return offset;
    }

    std::size_t Input::size() const {
        return s.size();
    }

    bool Input::isEof() const {
        return size() == 0;
    }

    std::string::value_type Input::head() const {
        return s.at(0);
    }

    std::pair<std::string::value_type, Input> Input::takeHead() {
        const auto h = head();
        return std::make_pair(h, (*this)++);
    }
}
