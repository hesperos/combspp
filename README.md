# Parser combinators in C++

This project is as example implementation of a concept of parser combinators in
C++ as originally described in [monadic parser combinators](https://www.cs.nott.ac.uk/~pszgmh/monparsing.pdf) paper.

## Building

    meson setup bld
    meson compile -C bld

## Example implementation

An example, fully functional INI file format parser can be found in
[examples](/examples) directory.  This implementation can be used as a
reference on how to use the `combspp` library.  It can be executed the
following way:

    ./bld/examples/inip/inip ./examples/inip/data/example.ini


## Details

This project is part of a [blog post](https://twdev.blog/2022/09/parser_combinators/) on [twdev.blog](https://twdev.blog).

You might be also interested in a Python prototype, implementing the same
concept which can be found [here](https://gitlab.com/hesperos/combs).
