#ifndef PARSERS_H
#define PARSERS_H

#include "input.h"

#include <functional>
#include <string>
#include <memory>

#include <combspp/input.h>
#include <combspp/result.h>

namespace combspp {

    template <typename A>
    using Parser = std::function<std::unique_ptr<Result<A>>(Input inp)>;


    template <typename A>
    Parser<A> makeZeroParser(std::string msg) {
        return [=](Input inp) {
            return std::make_unique<Error<A>>(msg, inp.getOffset());
        };
    }

    template <typename A>
    Parser<A> makeResultParser(A value) {
        return [=](Input inp) {
            return std::make_unique<Success<A>>(value, inp);
        };
    }

    Parser<char> makeCharParser() {
        return [=](Input inp) {
            if (inp.isEof()) {
                return makeZeroParser<char>("Unexpected end of input")(inp);
            }
            const auto [h, inp_next] = inp.takeHead();
            return makeResultParser<char>(h)(inp_next);
        };
    }

    template <typename A, typename B>
    using Binder = std::function<Parser<B>(A)>;

    template <typename A, typename B>
    Parser<B> bindParsers(Parser<A> a, Binder<A, B> b) {
        return [=](Input inp){
            auto resa = a(inp);
            auto parserb = b(resa->get());
            return parserb(resa->getRoi());
        };
    }

    Parser<char> makePredCharParser(std::function<bool(char)> pred) {
        return bindParsers<char, char>(
            makeCharParser(),
            [=](char c) -> Parser<char> {
                return pred(c) ?
                    makeResultParser(c) : makeZeroParser<char>("Predicate failed");
            });
    }

    template <typename T>
    Parser<T> makeAnyOf(Parser<T> a, Parser<T> b) {
        return [=](Input inp) {
            try {
                auto res = a(inp);
                res->get();
                return res;
            }
            catch (const std::runtime_error&) {
            }

            return b(inp);
        };
    }

    Parser<char> makeExactCharParser(char expected) {
        return makePredCharParser([=](auto c) { return c == expected; });
    }

    Parser<char> makeDigitParser() {
        return makePredCharParser([=](auto c) { return c >= '0' && c <= '9'; });
    }

    Parser<char> makeLowerParser() {
        return makePredCharParser([=](auto c) { return c >= 'a' && c <= 'z'; });
    }

    Parser<char> makeUpperParser() {
        return makePredCharParser([=](auto c) { return c >= 'A' && c <= 'Z'; });
    }

    Parser<char> makeSpaceParser() {
        return makePredCharParser([=](auto c) { return c == ' '; });
    }

    Parser<char> makeTabsParser() {
        return makePredCharParser([=](auto c) { return c == '\t'; });
    }

    Parser<char> makeNewlineParser() {
        return makePredCharParser([=](auto c) { return c == '\n' || c == '\r'; });
    }

    Parser<char> makeWhiteCharParser() {
        return makeAnyOf(
                makeAnyOf(
                    makeSpaceParser(),
                    makeTabsParser()),
                makeNewlineParser());
    }

    Parser<char> makeAlphaParser() {
        return makeAnyOf(
            makeLowerParser(),
            makeUpperParser());
    }

    Parser<char> makeAlphaNumParser() {
        return makeAnyOf(
            makeAlphaParser(),
            makeDigitParser());
    }

    template <typename A, typename B>
    using Reducer = std::function<B(A, B)>;

    template <typename A, typename B>
    Parser<B> makeManyTimesParser(Parser<A> pc, Reducer<A, B> reducer, B iv) {
        auto pe = makeResultParser(iv);
        auto ps = bindParsers<A, B>(pc, [=](auto c) -> Parser<B>{
                return bindParsers<B, B>(makeManyTimesParser(pc, reducer, iv),
                    [=](auto s) -> Parser<B>{
                        return makeResultParser(reducer(c, s));
                    });
       });

       return makeAnyOf(ps, pe);
    }

    Parser<std::string> makeManyTimesStringParser(Parser<char> pc) {
        return makeManyTimesParser<char, std::string>(pc,
            [](auto a, auto b) { return std::string(1, a) + b; },
            "");
    }

    Parser<std::string> makeWordParser() {
        return makeManyTimesStringParser(makeAlphaNumParser());
    }

    template <typename A>
    Parser<A> makeTakeRightParser(Parser<A> pa, Parser<A> pb) {
        return [=](Input inp) {
            const auto resa = pa(inp);
            return pb(resa->getRoi());
        };
    }

    template <typename A>
    Parser<A> makeTakeLeftParser(Parser<A> pa, Parser<A> pb) {
        return [=](Input inp) {
            const auto resa = pa(inp);
            const auto resb = pb(resa->getRoi());
            return makeResultParser(resa->get())(resb->getRoi());
        };
    }

    Parser<std::string> makePrefixParser(std::string prefix) {
        return [=](Input inp) {
            if (prefix.size() == 0) {
                return makeResultParser(std::string{})(inp);
            }

            return bindParsers<char, std::string>(
                makeExactCharParser(prefix.at(0)),
                [=](auto c) -> Parser<std::string>{
                    return bindParsers<std::string, std::string>(
                        makePrefixParser(prefix.substr(1)),
                        [=](auto s) -> Parser<std::string>{
                            return makeResultParser(std::string(1, c) + s);
                        });
                })(inp);
        };
    }

    template <typename A>
    Parser<A> makeOptionalParser(Parser<A> pa, A defaultValue = {}) {
        return [=](Input inp) {
            try {
                return pa(inp);
            }
            catch(const std::exception&) {
            }

            return makeResultParser(defaultValue)(inp);
        };
    }
}

#endif /* PARSERS_H */
