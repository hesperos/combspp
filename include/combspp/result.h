#ifndef RESULT_H
#define RESULT_H

#include <string>
#include <sstream>

#include <combspp/input.h>

namespace combspp {

    template <typename T>
    class Result {
    public:
        virtual ~Result() = default;

        virtual T get() const = 0;

        virtual Input getRoi() const = 0;
    };


    template <typename T>
    class Success : public Result<T> {
    public:
        explicit Success(T v, Input roi) :
            value{v},
            roi{roi}
        {
        }

        T get() const override {
            return value;
        }

        Input getRoi() const override {
            return roi;
        }

    private:
        T value;
        Input roi;
    };

    template <typename T>
    class Error : public Result<T> {
    public:
        explicit Error(std::string msg, std::size_t offset) :
            message{msg},
            offset{offset}
        {
        }

        T get() const override {
            std::stringstream ss;
            ss << offset << ": " << message;
            throw std::runtime_error(ss.str());
        }

        Input getRoi() const override {
            return {""};
        }

    private:
        std::string message;
        std::size_t offset;
    };
}

#endif /* RESULT_H */
