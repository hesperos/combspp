#ifndef INPUT_H
#define INPUT_H

#include <string>

#include <iostream>

namespace combspp {
    class Input {
    public:
        Input(std::string_view s);

        Input(const Input& other);

        Input& operator=(Input rhs);

        Input operator++(int);

        std::string_view get() const;

        std::size_t getOffset() const;

        std::size_t size() const;

        bool isEof() const;

        std::string::value_type head() const;

        std::pair<std::string::value_type, Input> takeHead();

    private:
        std::string_view s;
        std::size_t offset;
    };
}

#endif /* INPUT_H */
