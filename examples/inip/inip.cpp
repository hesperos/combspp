#include <combspp/parsers.h>

#include <algorithm>
#include <string>
#include <iterator>
#include <iostream>
#include <fstream>
#include <vector>

using KeyValue = std::pair<std::string, std::string>;

using KeyValues = std::vector<KeyValue>;

class Section {
public:
    Section(std::string name, KeyValues keyValues) :
        name{std::move(name)},
        keyValues{std::move(keyValues)}
    {
    }

    const KeyValues& getKeyValues() const {
        return keyValues;
    }

    std::string_view getName() const {
        return name;
    }

private:
    const std::string name;
    const KeyValues keyValues;
};

std::ostream& operator<<(std::ostream& os, const KeyValue& kv) {
    os << "{ " << kv.first << " -> " << kv.second << " }";
    return os;
}

std::ostream& operator<<(std::ostream& os, const KeyValues& kvs) {
    // This is a hack
    // std::ostream_iterator will not work unless operator<< is placed in std
    // namespace.
    std::transform(kvs.begin(),
            kvs.end(),
            std::ostream_iterator<std::string>(os, "\n"),
            [](auto kv){
                std::stringstream ss;
                ss << "  - " << kv;
                return ss.str();
            });
    return os;
}

std::ostream& operator<<(std::ostream& os, const Section& s) {
    os << "{Section: [" << s.getName() << "]\nKeyValues:\n";
    os << s.getKeyValues() << "}";
    return os;
}

using Sections = std::vector<Section>;

class Ini {
public:
    Ini(Sections sections) :
        sections{std::move(sections)}
    {
    }

    const Sections& getSections() const {
        return sections;
    }

private:
    const Sections sections;
};

std::ostream& operator<<(std::ostream& os, const Sections& s) {
    std::transform(s.begin(),
            s.end(),
            std::ostream_iterator<std::string>(os, "\n"),
            [](auto section) {
                std::stringstream ss;
                ss << section;
                return ss.str();
            });

    return os;
}

std::ostream& operator<<(std::ostream& os, const Ini& i) {
    os << i.getSections();
    return os;
}


combspp::Parser<std::string> makeWhiteCharParser() {
    return combspp::makeOptionalParser(
            combspp::makeManyTimesStringParser(
                combspp::makeWhiteCharParser()));
}

combspp::Parser<KeyValue> makeKeyValueParser(char delimiter = '=') {
    auto wordP = combspp::makeWordParser();
    auto whiteP = makeWhiteCharParser();
    auto delimP = combspp::makeExactCharParser(delimiter);

    auto strP = combspp::makeTakeRightParser(whiteP, wordP);
    auto delim2P = combspp::makeTakeRightParser(whiteP, 
            combspp::bindParsers<char, std::string>(delimP,
                [](auto c) {
                    return combspp::makeResultParser(std::string(1, c));
                }));

    auto kvP = combspp::bindParsers<std::string, KeyValue>(strP,
            [=](auto key) {
                return combspp::bindParsers<std::string, KeyValue>(delim2P, [=](auto) {
                            return combspp::bindParsers<std::string, KeyValue>(strP, [=](auto value) {
                                        return combspp::makeResultParser(
                                                std::make_pair(key, value));
                                    });
                        });
            });

    return kvP;
}

combspp::Parser<KeyValues> makeKeyValuesParser(char delimiter = '=') {
    return combspp::makeManyTimesParser<KeyValue, KeyValues>(
            makeKeyValueParser(delimiter),
            [=](auto a, auto b) {
                b.push_back(a);
                return b;
            },
            KeyValues{});
}

combspp::Parser<std::string> makeSectionNameParser() {
    auto wordP = combspp::makeWordParser();
    auto whiteP = makeWhiteCharParser();

    auto snameP = combspp::bindParsers<char, std::string>(
            combspp::makeExactCharParser('['),
            [=](auto) {
                return combspp::bindParsers<std::string, std::string>(
                        wordP, [=](auto sname) {
                            return combspp::bindParsers<char, std::string>(
                                    combspp::makeExactCharParser(']'),
                                    [=](auto) {
                                        return combspp::makeResultParser(sname);
                                    });
                        });
            });

    return combspp::makeTakeRightParser(whiteP, snameP);
}


combspp::Parser<Section> makeSectionParser() {
    auto snameP = makeSectionNameParser();
    auto sectionP = combspp::bindParsers<std::string, Section>(snameP,
            [=](auto sname) {
                return combspp::bindParsers<KeyValues, Section>(
                        makeKeyValuesParser(),
                        [=](auto kvs) {
                            return combspp::makeResultParser(Section{sname, kvs});
                        });
            });
    return sectionP;
}

combspp::Parser<Ini> makeSectionsParser() {
    auto sectionP = makeSectionParser();
    auto sectionsP = combspp::makeManyTimesParser<Section, Sections>(sectionP,
            [](auto a, auto b) {
                b.push_back(a);
                return b;
            },
            Sections{});

    return combspp::bindParsers<Sections, Ini>(sectionsP,
            [=](auto sections) {
                return combspp::makeResultParser(Ini{sections});
            });
}


combspp::Parser<Ini> makeIniParser() {
    return makeSectionsParser();
}

int main(int argc, const char* argv[]) {
    if (argc <= 1) {
        std::cerr << "inip <file>" << std::endl;
        return 1;
    }

    std::ifstream ifs{argv[1]};
    if (!ifs.good()) {
        std::cerr << "Unable to open input file: " << argv[1] << std::endl;
        return 1;
    }

    ifs.seekg(0, std::ios::end);
    const auto fileSize = ifs.tellg();
    ifs.seekg(0, std::ios::beg);

    std::string inp;
    inp.reserve(fileSize);
    inp.assign(std::istreambuf_iterator<char>(ifs),
            std::istreambuf_iterator<char>());

    auto p = makeIniParser();
    auto ini = p({inp});
    auto roi = ini->getRoi();

    std::cout << "ini file: " << ini->get() << std::endl;
    std::cout << "========" << std::endl;
    std::cout << "roi: " << roi.get() << std::endl;
    std::cout << "========" << std::endl;
    std::cout << "offset: " << roi.getOffset() << std::endl;

    return 0;
}
