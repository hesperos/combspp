#include <gtest/gtest.h>

#include <combspp/parsers.h>

class ParsersTest : public ::testing::Test {
public:
    void SetUp() {
    }

    void TearDown() {
    }
};

TEST_F(ParsersTest, test_ifCharParserConsumesSingleCharacter) {
    const auto inp = combspp::Input{"this is some input"};
    const auto expected = inp.head();

    auto p = combspp::makeCharParser();
    auto r = p(inp);

    ASSERT_EQ(expected, r->get());
    ASSERT_EQ(inp.get().substr(1), r->getRoi().get());
}

TEST_F(ParsersTest, test_ifReturnsErrorResultOnEmptyString) {
    const auto inp = combspp::Input{""};
    auto p = combspp::makeCharParser();
    auto r = p(inp);

    ASSERT_THROW(r->get(), std::runtime_error);
}

TEST_F(ParsersTest, test_ifExactCharParserWorksAsExpected) {
    auto pa = combspp::makeExactCharParser('a');
    auto pb = combspp::makeExactCharParser('b');

    ASSERT_EQ('a', pa({"abc"})->get());
    ASSERT_THROW(pa({"bcd"})->get(), std::runtime_error);

    ASSERT_EQ('b', pb({"bcd"})->get());
    ASSERT_THROW(pb({"abc"})->get(), std::runtime_error);
}

TEST_F(ParsersTest, test_ifAlphaParserWorksAsExpected) {
    auto p = combspp::makeAlphaParser();

    ASSERT_EQ('a', p({"abc"})->get());
    ASSERT_EQ('B', p({"BCD"})->get());
    ASSERT_THROW(p({"123"})->get(), std::runtime_error);
}

TEST_F(ParsersTest, test_ifAlphaNumParserWorksAsExpected) {
    auto p = combspp::makeAlphaNumParser();

    ASSERT_EQ('a', p({"abc"})->get());
    ASSERT_EQ('B', p({"BCD"})->get());
    ASSERT_EQ('2', p({"234abc"})->get());
}

TEST_F(ParsersTest, test_ifStringParserIsAbleToParseDigitSequences) {
    auto p = combspp::makeManyTimesStringParser(combspp::makeDigitParser());
    ASSERT_EQ("1234", p({"1234abc"})->get());
    ASSERT_EQ("", p({"abcd"})->get());
}

TEST_F(ParsersTest, test_ifWordParserAdvancesTheInputOffsetCorrectly) {
    auto p = combspp::makeWordParser();
    const auto res = p({"this is an input string"});
    const auto roi = res->getRoi();
    ASSERT_EQ("this", res->get());
    ASSERT_EQ(4, roi.getOffset());
}
